import { emailRegex } from "../../Utils/utils";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

function Login() {
  const navigate = useNavigate();
  const [value, setValue] = useState({
    password: "",
  });
  const [error, setError] = useState({
    email: "",
    password: "",
  });

  const registerUserDetails = localStorage.getItem("registerUser");

  const handleSubmit = (e) => {
    e.preventDefault();
    let errors = {};
    if (!value.email) {
      errors = { ...errors, email: "Email is required" };
    } else if (!emailRegex.test(value.email)) {
      errors = { ...errors, email: "Please Enter Valid Email" };
    }

    if (!value.password) {
      errors = { ...errors, password: "Password is required" };
    }

    setError(errors);
    let isInValid = Object.keys(errors).length;
    if (!isInValid) {
      let userDetails = JSON.parse(registerUserDetails);
      if (userDetails) {
        if (
          userDetails.email === value.email &&
          userDetails.password === value.password
        ) {
          localStorage.setItem("isLogin", true);
          navigate("/user", { replace: true });
          window.location.reload();
        } else {
          toast("User not found", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          console.log("User not found");
        }
      } else {
        toast("User not found");
        console.log("User not found");
      }
    }
  };

  const handleChange = (e) => {
    let { name, value } = e.target;
    setValue((previous) => ({ ...previous, [name]: value }));
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="inputField">
          <label>Email Id :</label>
          <input
            className="login-input"
            placeholder="Enter Email Id"
            name="email"
            type="text"
            onChange={handleChange}
            value={value.email}
          />
          <p className="error">{error.email}</p>
        </div>

        <div className="inputField">
          <label>Password :</label>
          <input
            className="login-input"
            placeholder="Enter Password"
            name="password"
            type="password"
            onChange={handleChange}
            value={value.password}
          />
          <p className="error">{error.password}</p>
        </div>

        <button className="login-btn" type="submit">
          Login
        </button>
      </form>
    </div>
  );
}

export default Login;
