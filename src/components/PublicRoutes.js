import React from 'react'
import { Navigate } from 'react-router-dom';

const isLogin = localStorage.getItem("isLogin")

const PublicRoutes = ({
    redirectPath = '/user',
    children,
  }) => {
    if (isLogin) {
      return <Navigate to={redirectPath} replace />;
    }
  
    return children;
  };

export default PublicRoutes