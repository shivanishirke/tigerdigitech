import React, { useEffect, useState } from 'react'
import axios from 'axios'
import UserDetails from './UserDetails'
import '../User/user.css'
import { FiSearch, FiFilter } from "react-icons/fi";

function User() {

    const [userDetails, setUserDetails] = useState([])
    const [singleUserDetails, setSingleUserDetails] = useState({})

    const getApi = async () => {
        try {
            let response = await axios.get("https://dummyjson.com/users")
            console.log('response', response)
            setUserDetails(response.data.users)
        } catch (error) {

        }

    }

    const setBg = () => {
        const randomColor = Math.floor(Math.random() * 16777215).toString(16);
        return `#${randomColor}`
    }

    const handleOpenUserDetails = (data) => {
        setSingleUserDetails(data)
    }

    useEffect(() => {
        getApi()
    }, [])

    return (
        <>
            <div className='user-container'>
                <div className='user-header'>
                    <div className='left-header'>
                        <p className='user-txt'>User</p>
                        <p>Here are all the users for this project</p>
                        <div className='header-search'>
                        <div className='search'>
                            <FiSearch />
                            <input
                                className='user-input'
                                name='search'
                                id='search'
                                placeholder='Search'
                            />
                        </div>
                        <div className='filter'>
                            <FiFilter/>
                            <p>Filter</p>
                        </div>
                        </div>
                    </div>
                    
                    <div className='right-header'>
                        <button className='add-btn'>Add User</button>
                    </div>


                </div>

                <table className='main-table'>
                    <thead className='thead-1'>
                        <tr className='table-tr'>
                            <th className='table-th'>Name</th>
                            {/*  <th className='table-th'>Name</th> */}
                            <th className='table-th'>Gender</th>
                            <th className='table-th'>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        {userDetails.map((data) => {
                            const borderColour = setBg()
                            return (
                                <tr className='table' key={data.id}>
                                    <td style={{ borderColor: borderColour }}>
                                        <div className='flex' onClick={() => handleOpenUserDetails(data)}>
                                            <img className='user-img' src={data.image} />
                                            <span className='name'>{data.firstName} {data.lastName}</span>
                                        </div>
                                    </td>

                                    <td className='table-td'>{data.gender}</td>
                                    <td className='table-td' >{data.email}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>

                {Object.keys(singleUserDetails).length?  <UserDetails
                    userDetails={singleUserDetails}
                    setClose={setSingleUserDetails}
                />: ''}
            </div>


        </>
    )
}

export default User