import React from "react";
import { CiUser } from "react-icons/ci";
import { RxCross1 } from "react-icons/rx";

function UserDetails({ userDetails,setClose }) {


  const handleClick = () => {
    setClose({})
  }
  console.log(userDetails);
  return (
    <div className="userDetails-container">
      <div className="userDetails-bg">
        <h1 className="userDetails-h1">User Details</h1>
       <span className='userDetails-span'  onClick={handleClick}><RxCross1 /></span> 
        <div className="userDetails-content">
          <img className='userDetails-img1'  src={userDetails.image} />

          <div >
            <p className="userDetails-p1">
              {userDetails.firstName} {userDetails.lastName}
            </p>
            <p className="userDetails-p2">User Name : {userDetails.username}</p>
            <button className="userDetails-btn">Active</button>
          </div>
        </div>

        <div>
          
          <p> <CiUser /> Basic and account details :</p>
          <p className="para-userDetails">{userDetails.firstName} {userDetails.lastName}</p>
          <p className="userDetails-name">Full Name</p>

          <p className="para-userDetailsEmail">{userDetails.gender}</p>
          <p className="userDetails-Email">Gender</p>

          <p className="para-userDetailsEmail">{userDetails.email}</p>
          <p className="userDetails-Email">Email</p>
        </div>

      </div>
    </div>
  );
}

export default UserDetails;
