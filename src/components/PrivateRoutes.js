import React from 'react'
import { Navigate } from 'react-router-dom';

const isLogin = localStorage.getItem("isLogin")


const PrivateRoutes = ({
    redirectPath = '/login',
    children,
  }) => {
    const isRegister = localStorage.getItem("registerUser")
    if(!isRegister){
         return <Navigate to={'/register'} replace />
    }
    if (!isLogin) {
      return <Navigate to={redirectPath} replace />;
    }
  
    return children;
  };

export default PrivateRoutes