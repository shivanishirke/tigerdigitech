import { emailRegex } from "../../Utils/utils";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

function Registration() {
  const navigate = useNavigate();
  const [value, setValue] = useState({
    email: "",
    userName: "",
    password: "",
  });
  const [error, setError] = useState({
    email: "",
    userName: "",
    password: "",
  });

  const handleSubmit = (e) => {
    e.preventDefault()
    let errors = {};
    if (!value.email) {
      errors = { ...errors, email: "Email is required" };
    } else if (!emailRegex.test(value.email)) {
      errors = { ...errors, email: "Please Enter Valid Email" };
    }

    if (!value.password) {
      errors = { ...errors, password: "Password is required" };
    }

    if (!value.userName) {
      errors = { ...errors, userName: "userName is required" };
    }
    setError(errors)
    let isInValid = Object.keys(errors).length
    if(!isInValid){
      localStorage.setItem("registerUser",JSON.stringify(value))
      navigate('/login', { replace: true });
    }
  };

  const handleChange = (e) => {
    let { name, value } = e.target;
    setValue((previous) => ({ ...previous, [name]: value }));
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
      <div className="inputField">
      <label>UserName :</label>
      <input
        className="login-input"
        placeholder="Enter UserName"
        name="userName"
        type="text"
        value={value.userName}
        onChange={handleChange}
      />
      <p className="error">{error.userName}</p>
      </div>
       

      <div className="inputField">
      <label>Email Id :</label>
      <input
      className="login-input"
        placeholder="Enter Email Id"
        name="email"
        type="text"
        onChange={handleChange}
        value={value.email}
      />
      <p className="error">{error.email}</p>
      </div>

      <div className="inputField">
      <label>Password :</label>
      <input
      className="login-input"
        placeholder="Enter Password"
        name="password"
        type="password"
        onChange={handleChange}
        value={value.password}
      />
      <p className="error">{error.password}</p>
      </div> 

      
        <button className="login-btn" type="submit">Register</button>
      </form>
    </div>
  );
}

export default Registration;
