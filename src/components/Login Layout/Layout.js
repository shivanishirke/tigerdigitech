import React from 'react'
import { Outlet } from 'react-router-dom'
import '../Login/login.css'

function Layout(props) {
  return (
     <div className='layout-container'>
                <div className='left-screen'>
                    
                </div>
                <div className='right-screen'>
                            <img className='logo1' src='./loginlogo.PNG'></img>
                            <p className='para1'>WELCOME</p>
                            <p className='para2'>{props.pageName} to Labs Monitoring System</p>
                            <div className='login-form'><Outlet/></div>
                </div>
     </div>
  )
}

export default Layout