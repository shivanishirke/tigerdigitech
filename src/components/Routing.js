import * as React from "react";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Layout from "./Login Layout/Layout";
import Login from "./Login/Login";
import PrivateRoutes from "./PrivateRoutes";
import PublicRoutes from "./PublicRoutes";
import Registration from "./Registration/Registration"
import User from "./User/User";




export const router = createBrowserRouter([
  {
    element: <Layout/>,
    children: [
      {
        path: "/login",
        element: <PublicRoutes><Login/></PublicRoutes>,
      },
      {
        path: "/register",
        element:  <PublicRoutes><Registration/></PublicRoutes>,
      },
      
    ]
  },
  {
    path: "/user",
    element: <PrivateRoutes><User/></PrivateRoutes>,
  },
  {
    path: "/",
    element: <PrivateRoutes></PrivateRoutes>,
  },
]);