import logo from './logo.svg';
import { useEffect } from "react";
import './App.css';
import Layout from './components/Login Layout/Layout'

function App() {
  return (
    <div className="App">
      <header className="App-header">
         <Layout/>
      </header>
    </div>
  );
}

export default App;
